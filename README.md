# README #

This project is exercise about how to build a real project with good architecture.

### What is this base foundation? ###

* Constants
* Network
* Base View
* Base Protocol
* Common View
* Appearance
* Animator
* Utils
* [x] Secuirty
* [x] JSBridge
* [x] Cache
* [x] Notification center
* [x] Auth System
* [x] User Track
* [x] Monitor

### Architecture Concept:

* [AppScaffold v1 -- Scaffold](https://www.lucidchart.com/publicSegments/view/bed4a7e1-7ff1-44ec-bcdf-dc2781f79248/image.jpeg)
* [AppScaffold v2 -- Components](https://www.lucidchart.com/publicSegments/view/69699495-89e7-4e9b-8d0e-a582d89462a5/image.jpeg)
* [AppScaffold v3 -- Plugins](https://www.lucidchart.com/publicSegments/view/934b48e8-3833-4808-bc57-c67fd120363f/image.jpeg)

### Third libraries? ###

`layout`

* SnapKit

`Network`

* Alamofire
* ReachabilitySwift

`Image`

* SDWebImage

`Security`

* CryptoSwift

`Mapping`

* ObjectMapper

`UX`

* CSStickyHeaderFlowLayout
* DGElasticPullToRefresh

### CI ###

* Writing tests
* Code review
* [x] Fastlane