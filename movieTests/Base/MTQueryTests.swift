//
//  MTQuery.swift
//
//

import XCTest
import ObjectMapper

@testable import movie

class MTQueryTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchMovieData() {
        let params = ["page": "1"]
        let expectation = self.expectation(description: "MTQuery")
        
        MTQuery.shared.getObject(.discover, params: params, completionHandler: { (object, error) -> Void in
            
            XCTAssertNil(error)
            XCTAssertNotNil(object)
            
            let results = Mapper<Result<Movie>>().map(JSONString: object!)?.results
            
            XCTAssertTrue(results!.count > 0, "the results should contain some items")
          
            expectation.fulfill()
            
        })
        
        waitForExpectations(timeout: 10) { (error) in
            XCTAssertNil(error)
        }
    }
    
    func testFetchMovieDetailData() {
        
        let params = ["id": 321612]
        let expectation = self.expectation(description: "MTQuery")
        
        MTQuery.shared.getObject(.movie, params: params, completionHandler: { (object, error) -> Void in
            
            XCTAssertNil(error)
            XCTAssertNotNil(object)
            
            let result = Mapper<Movie>().map(JSONString: object!)
            
            XCTAssertNotNil(result)
            
            expectation.fulfill()
            
        })
        
        
        waitForExpectations(timeout: 10) { (error) in
            XCTAssertNil(error)
        }
    }
}
