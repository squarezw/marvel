//
//  MTCollectionViewCell.swift
//
//

import UIKit

class MTCollectionSectionTitleView: UICollectionViewCell {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        addSubview(titleLabel)
        self.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        setupConstraints()
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.centerYWithinMargins.equalToSuperview()
            make.height.equalTo(25)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
    }
}

// this is loading view in the Collection Footer View
class MTCollectionReusableView: UICollectionReusableView {
    lazy var indicator: UIActivityIndicatorView = {
        return UIActivityIndicatorView(activityIndicatorStyle: .white)
    }()
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        addSubview(indicator)
        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        indicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
