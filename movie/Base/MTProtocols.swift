//
//  MTProtocols.swift
//
//

import UIKit

// Presenter <-> View
protocol ViewPrototol: class {
    func reloadData()
    func isFetchingData()
    func completedFetch()
}


// Common Image Cell Protocol
protocol MImageCellModelProtocol {
    var title: String { get set }
    var poster: String { get set }
    var popularityDesc: String { get set }
    var posterUrl: URL? { get }
}

extension MImageCellModelProtocol {
    var posterUrl: URL? {
        if let url = URL(string: poster) {
            return url
        }
        return nil
    }
}
