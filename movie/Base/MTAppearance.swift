//
//  MTAppearace.swift
//
//

import UIKit

class MTAppearance {
    static let tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let searchBarTintColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
    static let lightTextColor = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)

    static func setup() {
        self.customNavigationAppearance()
        self.customTableViewAppearance()
    }
    
    static func customNavigationAppearance() {
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = tintColor
        navigationBarAppearace.barTintColor = searchBarTintColor
        
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    static func customTableViewAppearance() {
        let tableViewAppearace = UITableView.appearance()
        tableViewAppearace.sectionIndexBackgroundColor = UIColor.clear
        tableViewAppearace.backgroundColor = UIColor.white
    }
}

