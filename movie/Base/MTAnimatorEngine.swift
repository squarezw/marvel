//
//  MTAnimatorEngine.swift
//
//

import UIKit

class MTAnimatorEngine: NSObject, UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate
{
    static let shared = MTAnimatorEngine()
    
    var isPresenting = true
    fileprivate var isNavPushPop = false
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 0.35
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        if isPresenting { showOptions(using:transitionContext) }
        else            { hideOptions(using:transitionContext) }
    }
    
    func showOptions(using context: UIViewControllerContextTransitioning)
    {
        guard let fromVC = context.viewController(forKey: .from),
            let toVC = context.viewController(forKey: .to),
            let toView   = context.view(forKey: .to) else {
                return
        }
        
        let containerView = context.containerView
//        let finalFrame = context.finalFrame(for: toVC)
//        let snapshot = toVC.view.snapshotView(afterScreenUpdates: true)
        
        containerView.addSubview(toVC.view)
        
        let frame = context.finalFrame(for: context.viewController(forKey: .to)!)
        
        toView.frame = frame
        toView.layer.position = CGPoint(x: containerView.frame.origin.x, y: containerView.frame.origin.y + frame.origin.y)
        toView.layer.anchorPoint = CGPoint(x:containerView.frame.origin.x,y:containerView.frame.origin.y)
        toView.transform = toView.transform.scaledBy(x: 1.0, y: 0.01)
        containerView.addSubview(toView)
        
        UIView.animate(withDuration: 0.35, animations: { toView.transform = .identity } )
        {
            (finished)->Void in
            fromVC.view.transform = .identity
            context.completeTransition( !context.transitionWasCancelled )
        }
    }
    
    func hideOptions(using context: UIViewControllerContextTransitioning)
    {
        guard let fromView = context.view(forKey: .from),
            let toView   = context.view(forKey: .to) else {
                return
        }
        
        let container = context.containerView
        container.insertSubview(toView, belowSubview: fromView)
        fromView.layer.anchorPoint = CGPoint(x:container.frame.origin.x,y:container.frame.origin.y)
        toView.frame = context.finalFrame(for: context.viewController(forKey: .to)!)
        toView.layoutIfNeeded()
        UIView.animate(withDuration: 0.35, animations:
            { fromView.transform = fromView.transform.scaledBy(x: 1.0, y: 0.01) } )
        {
            (finished)->Void in
            
            context.completeTransition( !context.transitionWasCancelled )
        }
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        isPresenting = false
        return self
    }
    
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        isPresenting = true
        return self
    }
}

extension MTAnimatorEngine: UINavigationControllerDelegate
{
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        isNavPushPop = true
        self.isPresenting = operation == .push
        return self
    }
}
