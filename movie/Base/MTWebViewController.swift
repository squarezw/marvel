//
//

import UIKit

class MTWebViewController: MTViewController, UIWebViewDelegate {
    
    var url: String
    
    lazy var webView: UIWebView = {
        let webView = UIWebView(frame: self.view.bounds)
        webView.delegate = self
        return webView
    }()
    
    lazy var indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        indicator.center = self.view.center
        return indicator
    }()
    
    init(url: String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let validatedUrl = url.trimmingCharacters(in: CharacterSet.whitespaces)
        if let URL = URL(string: validatedUrl) {
            let request = NSMutableURLRequest(url: URL)
            webView.loadRequest(request as URLRequest)
            view.addSubview(webView)
            view.addSubview(indicator)
        }
    }
    
    // UIWebViewDelegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        indicator.stopAnimating()
        let title = webView.stringByEvaluatingJavaScript(from: "document.title")
        self.title = title
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        indicator.stopAnimating()
    }
}
