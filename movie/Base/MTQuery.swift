//
//  MTQuery.swift
//
//

import UIKit
import ObjectMapper
import CryptoSwift

typealias APIResources = APIConfig.Resources

// API Config
struct APIConfig {
    
    static let publicKey: String = "af991749a06b35df8345231a0d86c327" //"5d471eec06fb78b2156d8060d0c545ef"
    static let privateKey: String = "9da23040dd7f528dde553000691db70bb7076427"
    static let domain: String = "api.themoviedb.org" // "gateway.marvel.com"
    static let apiVersion: String = "3/" // "v1/public/"
    static let host: String = "https://\(domain)/\(apiVersion)"
    
    static func baseParams() -> [String: String] {
        let ticks: String = String(Date().ticks)
        let hash: String = "\(ticks)\(privateKey)\(publicKey)".md5()
        return ["api_key": publicKey, "ts": ticks, "hash": hash]
    }
    
    // Resources
    enum Resources: String {
        case characters = "characters"
        case comics = "comics"
        case discover = "discover/movie"
        case movie = "movie/{id}"
        
        var uri: String {
            return APIConfig.host + self.rawValue
        }
        
        func uri(withId id: String) -> String {
            return APIConfig.host + self.rawValue.replacingOccurrences(of: "{id}", with: id)
        }
        
    }
}

// Image path combine
extension APIConfig {
    static let imageHost: String = "https://image.tmdb.org/t/p/w500"
    static func fullImageUrlString(imagePath path: String) -> String {
        return imageHost + path
    }
}

extension MTError {
    init(errorCode code: Int) {
        self.init(domain: APIConfig.domain, code: code)
    }
    
    init(internalError error: MTInternalError) {
        self.init(domain: APIConfig.domain, code: error.rawValue)
    }
}

class MTQuery: NSObject {
    static var shared = MTQuery()
    let session: URLSession = URLSession.shared
    
    let host: String = APIConfig.host
    
    func getObject(_ api: APIConfig.Resources, params: MTDictionary?, completionHandler: @escaping MTQueryStringResultClosure) {
        fetchData(request: request(fullUri(prefix: api.uri, params: params)), completionHandler: completionHandler)
    }
    
    func getObject(_ uri: String, params: MTDictionary?, completionHandler: @escaping MTQueryStringResultClosure) {
        fetchData(request: request(fullUri(prefix: uri, params: params)), completionHandler: completionHandler)
    }
    
    func fetchData(request: URLRequest, completionHandler: @escaping MTQueryStringResultClosure) {
        #if DEBUG
        print(request)            
        #endif
        
        // checking network is connected
        if (isReachableWithServer()) {
            // Network fetching
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                
                var dataString: String?
                
                if let data = data {
                    dataString = String(data: data, encoding: String.Encoding.utf8)
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                DispatchQueue.main.sync {
                    completionHandler(dataString, MTQuery.error(response: response, data: data?.jsonObject))
                }
                
            })
            
            task.resume()
            
        }
    }
    
    private func fullUri(prefix: String, params: MTDictionary? = nil) -> String {
        var tmpParams: MTDictionary = APIConfig.baseParams()
        if let p = params {
            tmpParams.update(p)
        }
        
        return prefix + "?" + tmpParams.stringWithParams
    }
    
    func request(_ uri: String) -> URLRequest {
        let url: URL = URL(string: uri)!
        let request = NSMutableURLRequest(url: url)
        return request as URLRequest
    }
    
    // !!!: it is fake
    func isReachableWithServer() -> Bool {
        return true
    }
    
    public static func error(response: URLResponse?, data: [String: Any]?) -> MTError? {
        guard let response = response as? HTTPURLResponse, response.statusCode > 400 else {
            return nil
        }
        var error = MTError(errorCode: response.statusCode)
        if let data = data, let status = data["status"] as? String {
            error.status = status
        }
        return error
    }
    
}

enum SortType: Int {
    case desc
    case asc
    
    var value: String {
        switch self {
        case .desc: return "name"
        case .asc: return "-name"
        }
    }
    
}
