//
//  MTViewController.swift
//
//

import UIKit
import SnapKit
import CSStickyHeaderFlowLayout
import DGElasticPullToRefresh

class MTViewController: UIViewController, ViewPrototol {
    lazy var activityIndicator = {
        return UIActivityIndicatorView(activityIndicatorStyle: .white)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
    }
    
    func reloadData() {}
    
    func isFetchingData() {
        activityIndicator.startAnimating()
    }
    
    func completedFetch() {
        activityIndicator.stopAnimating()
    }
}

class MTCollectionViewController: MTViewController
{
    let defaultUICollectionViewCell = "UICollectionViewCell"
    let defaultSectionHeaderIdentifier = "SectionHeaderIdentifier"
    let defaultSectionFooterIdentifier = "SectionFooterIdentifier"
    
    var allowPullRefresh: Bool = true
    
    let layout: CSStickyHeaderFlowLayout = {
        let layout = CSStickyHeaderFlowLayout()
        return layout
    }()
    
    lazy var collectionView: UICollectionView = {
        let view: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.layout)
        view.alwaysBounceVertical = true
        
        if self.allowPullRefresh == true {
            let loadingView = DGElasticPullToRefreshLoadingViewCircle()
            loadingView.tintColor = UIColor(red: 78/255.0, green: 221/255.0, blue: 200/255.0, alpha: 1.0)
            view.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
                self?.refresh()
                self?.collectionView.dg_stopLoading()
                }, loadingView: loadingView)
            view.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
            view.dg_setPullToRefreshBackgroundColor(view.backgroundColor!)
        }
        return view
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 10, width: 52, height: 44))
        button.contentMode = UIViewContentMode.center
        button.setTitle("⬅️", for: .normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
        collectionView.delegate = self
        collectionView.dataSource = self
        configCollectionView()
    }
    
    func configCollectionView() {
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: defaultUICollectionViewCell)
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: defaultSectionHeaderIdentifier)
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: defaultSectionFooterIdentifier)
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: CSStickyHeaderParallaxHeader, withReuseIdentifier: CSStickyHeaderParallaxHeader)
    }
    
    override func reloadData() {
        collectionView.reloadData()
    }
    
    func refresh() {}
    
    func loadMore() {}
    func loadMore(lastIndex index: Int) {}
    
    func backButtonPressed() { self.navigationController?.popViewController(animated: true) }
    
    deinit {
        collectionView.dg_removePullToRefresh()
    }
}

extension MTCollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        loadMore(lastIndex: indexPath.row)
    }
}

extension MTCollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: defaultUICollectionViewCell, for: indexPath)
        return cell
    }
    
    // Cell Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize.zero
    }
    
    // Cell Interitem Spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    // Cell Line Spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    // CollectionView Inset
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    // Header
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    // Footer
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let supplementaryView: UICollectionReusableView
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: defaultSectionHeaderIdentifier, for: indexPath)
        case UICollectionElementKindSectionFooter:
            supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: defaultSectionFooterIdentifier, for: indexPath)
        case CSStickyHeaderParallaxHeader:
            supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CSStickyHeaderParallaxHeader, for: indexPath)
        default:
            supplementaryView = UICollectionReusableView()
        }
        return supplementaryView
    }
}
