//
//  MTConstants.swift
//
//

import Foundation

// ... Policies

/**
 Such as Cache Policies
 */


// ... Logging Levels


// Errors

// Internal error status
public enum MTInternalError: Int {
    case badParse  = 3001
    case badParams = 3002
    case badResponse = 3003
}

public struct MTError : Error {
    let domain: String
    let code: Int
    var status: String?
    
    var localizedDescription: String {
        return "[\(domain)] code:\(code) " + (status ?? "")
    }
    
    init(domain: String, code: Int) {
        self.domain = domain
        self.code = code
    }
    
    init(domain: String, internalError error: MTInternalError) {
        self.init(domain: domain, code: error.rawValue)
    }
}

/** 
 k<#NameSpace#>ErrorMissingObjectId
 k<#NameSpace#>ErrorObjectNotFound
 k<#NameSpace#>ErrorInvalidJSON
 */

// ... Blocks

public typealias MTDictionary = [String: Any]
public typealias MTQueryDictionaryResultClosure = (_ objects: MTDictionary?, _ error: MTError?) -> Void
public typealias MTQueryStringResultClosure = (_ objects: String?, _ error: MTError?) -> Void


// ... Notifications

/**
 Network Notifications
 
 extern NSString *const _Nonnull PFNetworkWillSendURLRequestNotification;
 */


// ... Macros

/**
 Deprecated Macros
 Extensions Macros
 
 #ifndef PF_SWIFT_UNAVAILABLE
 #  ifdef NS_SWIFT_UNAVAILABLE
 #    define PF_SWIFT_UNAVAILABLE NS_SWIFT_UNAVAILABLE("")
 #  else
 #    define PF_SWIFT_UNAVAILABLE
 #  endif
 #endif
 */

