//
//  MTHelper.swift
//  movie
//
//

import Foundation

public enum Switch{
    case On
    case Off
    
    mutating func click(){
        switch self{
        case .On:
            self = .Off
        case .Off:
            self = .On
        }
    }
}
