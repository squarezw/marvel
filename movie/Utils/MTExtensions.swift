//
//  MTExtensions.swift
//
//

import UIKit

// MARK: Data
extension Data {
    var jsonObject: [String: Any]? {
        let json = try? JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableContainers)
        
        if let result = json as? [String: Any] {
            return result
        }
        
        return nil
    }
}


// MARK: Date
extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
    
    init(ticks: UInt64) {
        self.init(timeIntervalSince1970: Double(ticks)/10_000_000 - 62_135_596_800)
    }
}

// MARK: Dictionary
extension Dictionary {
    var stringWithParams: String {
        return self.map {"\($0)=\($1)"}.joined(separator: "&")
    }
    
    mutating func update(_ dict: Dictionary) {
        for (key, value) in dict {
            self.updateValue(value, forKey: key)
        }
    }
}

extension String {
    func height(_ font: UIFont, constrainedWidth: CGFloat, maxHeight: CGFloat = .greatestFiniteMagnitude) -> CGFloat {
        let constraintRect = CGSize(width: constrainedWidth, height: maxHeight)
        let frame = self.boundingRect(with: constraintRect, options: [.usesFontLeading, .usesLineFragmentOrigin], attributes: [NSFontAttributeName: font], context: nil)
        return frame.height
    }
}

// MARK: UIView
extension UIView {
    // find responder view controller
    func responderViewController() -> UIViewController {
        var responder: UIResponder! = nil
        var next = self.superview
        while next != nil {
            responder = next?.next
            if (responder!.isKind(of: UIViewController.self)){
                return (responder as! UIViewController)
            }
            next = next?.superview
        }
        return (responder as! UIViewController)
    }
}
