//
//  MCharaterListPresenter.swift
//
//

import Foundation

class MCharaterListPresenter {
    weak var view: ViewPrototol?
    weak var router: MTRoute?
    var interactor: MCharaterListInteractor
    var items: [MImageCellModelCharacter]?
    var page: Int = 1
    var limit: Int = 20
    
    var isLoading: Bool {
        get {
            return self.interactor.isLoading
        }
    }
    var isLastPage: Bool {
        get {
            return self.interactor.isLastPage
        }
    }
    
    // MARK: Initialize
    init(interactor: MCharaterListInteractor) {
        self.interactor = interactor
    }
    
    // MARK: public methods
    public func viewIsReady() {
        // 2
        self.interactor.fetchData()
    }
    
    public func loadMore(currentIndex index: Int) {
        // close to last index
        if page * limit - 5 < index {
            page += 1
            self.interactor.loadMore(page: page, limit: limit)
        }
    }
    
    public func refresh() {
        page = 1
        self.interactor.fetchData()
    }
    
    public func orderBy(_ key: String) {
        interactor.orderBy(key)
        view?.isFetchingData()
    }
    
    public func searchBy(_ key: String) {
        interactor.searchBy(key)
        view?.isFetchingData()
    }
    
    // 4
    public func setData(items: [Character]) {
        self.items = self.convertItems(items: items)
        view?.reloadData()
        view?.completedFetch()
    }
    
    public func appendData(items: [Character]) {
        self.items?.append(contentsOf: self.convertItems(items: items))
        view?.reloadData()
        view?.completedFetch()
    }
    
    // favourite
    public func favourite(index: Int) {
        if let character = items?[index] {
            interactor.favourite(character: character.object)
        }
    }
    
    public func gotoDetailPage(index: Int) {
        guard let character = items?[index] else {
            return
        }
        
        self.router?.gotoDetailPage(character: character.object)
    }
    
    // MARK: private
    private func convertItems(items: [Character]) -> [MImageCellModelCharacter] {
        var array: [MImageCellModelCharacter] = [MImageCellModelCharacter]()
        for item in items {
            guard let urlString = item.thumbnail?.fullUrl(type: ImageVariants.portraitUncanny),
                let name = item.name else {
                    continue
            }
            array.append(MImageCellModelCharacter(title: name, poster: urlString, popularityDesc: item.desc ?? "", object: item))
        }
        return array
    }
    
}
