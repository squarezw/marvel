//
//  MCharaterListPresenter.swift
//
//

import Foundation

class MDiscoverListPresenter {
    weak var view: ViewPrototol?
    weak var router: MTRoute?
    var interactor: MDiscoverListInteractor
    var items: [MImageCellModelMovie] = []
    var page: Int = 1
    
    var isLoading: Bool {
        get {
            return self.interactor.isLoading
        }
    }
    var isLastPage: Bool {
        get {
            return self.interactor.isLastPage
        }
    }
    
    // MARK: Initialize
    init(interactor: MDiscoverListInteractor) {
        self.interactor = interactor
    }
    
    // MARK: public methods
    public func viewIsReady() {
        // 2
        self.interactor.fetchData()
    }
    
    public func loadMore(currentIndex index: Int) {
        // close to last index
        if !isLastPage && !isLoading && items.count - 5 < index {
            page += 1
            self.interactor.loadMore(page: page)
        }
    }
    
    public func refresh() {
        page = 1
        self.interactor.fetchData()
    }
    
    public func orderBy(_ key: String) {
        interactor.orderBy(key)
        view?.isFetchingData()
    }
    
    public func searchBy(_ key: String) {
        interactor.searchBy(key)
        view?.isFetchingData()
    }
    
    // 4
    public func setData(items: [MovieBase]) {
        self.items = self.convertItems(items: items)
        view?.reloadData()
        view?.completedFetch()
    }
    
    public func appendData(items: [MovieBase]) {
        self.items.append(contentsOf: self.convertItems(items: items))
        view?.reloadData()
        view?.completedFetch()
    }
    
    public func loadDataError(error: MTError) {
        // TODO: error alert
        view?.completedFetch()
    }
    
    public func gotoDetailPage(index: Int) {
        let object = items[index]
        
        self.router?.gotoMovieDetailPage(movie: object.object)
    }
    
    // MARK: private
    private func convertItems(items: [MovieBase]) -> [MImageCellModelMovie] {
        var array: [MImageCellModelMovie] = [MImageCellModelMovie]()
        for item in items {
            guard let urlString = item.posterPath,
                let name = item.title else {
                    continue
            }
            array.append(MImageCellModelMovie(title: name, poster: urlString, popularityDesc: item.overview ?? "", object: item))
        }
        return array
    }
    
}
