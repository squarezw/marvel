//
//  MovieSortHeaderView.swift
//
//

import UIKit

class MovieSortHeaderView: UICollectionReusableView, UISearchBarDelegate {
    weak var present: MDiscoverListPresenter?    
    
    lazy var searchBar: UISearchBar = {
        let bar = UISearchBar()
        bar.delegate = self
        return bar
    }()
    
    lazy var toolbar : UIToolbar = {
        let bar = UIToolbar(frame: CGRect.zero)
        let item1 = UIBarButtonItem(title: "🔼 Name", style: .plain, target: self, action: #selector(sort))
        item1.tag = SortType.desc.rawValue
        item1.width = self.frame.width / 2 - 30
        let item2 = UIBarButtonItem(title: "🔽 Name", style: .plain, target: self, action: #selector(sort))
        item2.tag = SortType.asc.rawValue
        item2.width = self.frame.width / 2 - 30
        bar.setItems([item1, item2], animated: false)
        bar.barTintColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        return bar
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        addSubview(toolbar)
        addSubview(searchBar)
        
        toolbar.snp.makeConstraints { (make) in
            make.height.equalTo(self)
            make.width.equalTo(self.frame.width)
            make.center.equalToSuperview()
        }
        
        searchBar.snp.makeConstraints { (make) in
            make.top.equalTo(-30)
            make.height.equalTo(30)
            make.left.right.equalToSuperview()
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if point.y < 0 && point.y > -searchBar.frame.height {
            return searchBar
        }
        return super.hitTest(point, with: event)
    }
    
    func sort(sender: UIBarButtonItem) {
        guard let present = present else {
            return
        }
        
        toolbar.items?.forEach({ (item) in
            if item == sender {
                item.style = .done
            } else {
                item.style = .plain
            }
        })
        
        switch sender.tag {
        case SortType.desc.rawValue:
            present.orderBy(SortType.desc.value)
        case SortType.asc.rawValue:
            present.orderBy(SortType.asc.value)
        default: break
        }
    }
    
    // UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        present?.searchBy(text)
    }
}
