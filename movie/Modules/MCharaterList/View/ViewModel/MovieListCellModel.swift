//
//  MovieListCellModel.swift
//
//

import Foundation

struct MImageCellModel: MImageCellModelProtocol {
    var title: String
    var poster: String
    var popularityDesc: String
    
    init(title: String) {
        self.init(title: title, poster: "", popularityDesc: "")
    }
    
    init(title: String, poster: String, popularityDesc: String) {
        self.title = title
        self.poster = poster
        self.popularityDesc = popularityDesc
    }
}

struct MImageCellModelCharacter: MImageCellModelProtocol {
    var title: String
    var poster: String
    var popularityDesc: String
    var object: Character
    var isFavourite: Bool {
        return object.isFavourite == 1
    }
}

struct MImageCellModelMovie: MImageCellModelProtocol {
    var title: String
    var poster: String
    var popularityDesc: String
    var object: MovieBase
    
    init(title: String, poster: String, popularityDesc: String, object: MovieBase) {
        self.title = title
        self.poster = APIConfig.fullImageUrlString(imagePath: poster)
        self.popularityDesc = popularityDesc
        self.object = object
    }
}
