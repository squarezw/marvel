//
//  MovieListViewController.swift
//

import UIKit
import CSStickyHeaderFlowLayout
import DGElasticPullToRefresh

class MCharaterListViewController: MTCollectionViewController {
    
//    var presenter: MCharaterListPresenter
    var presenter: MDiscoverListPresenter
    
    var isAppearSearchBar: Bool = false
    
    init(presenter: MDiscoverListPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configCollectionView() {
        super.configCollectionView()
        layout.parallaxHeaderAlwaysOnTop = true
        layout.parallaxHeaderReferenceSize = CGSize(width: self.view.frame.width, height: 50)
        layout.parallaxHeaderMinimumReferenceSize = CGSize(width: self.view.frame.width, height: 40)
        collectionView.register(MCharaterListCell.self, forCellWithReuseIdentifier: defaultUICollectionViewCell)
        collectionView.register(MTCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: defaultSectionFooterIdentifier)
        collectionView.register(MovieSortHeaderView.self, forSupplementaryViewOfKind: CSStickyHeaderParallaxHeader, withReuseIdentifier: CSStickyHeaderParallaxHeader)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MARVEL"
        
        // 1
        presenter.viewIsReady()
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(appearSearchBar))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    override func loadMore(lastIndex index: Int) {
        presenter.loadMore(currentIndex: index)
    }
    
    override func refresh() {
        presenter.refresh()
    }
    
    func appearSearchBar() {
        
        collectionView.spring(animations: { 
            
            if self.isAppearSearchBar {
                self.collectionView.top = 0
            } else {
                self.collectionView.top = 30
            }            
            
        }) { (finished) in
            self.isAppearSearchBar = !self.isAppearSearchBar
        }
        
    }
}

// UICollectionViewDataSource
extension MCharaterListViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MCharaterListCell = collectionView.dequeueReusableCell(withReuseIdentifier: defaultUICollectionViewCell, for: indexPath) as! MCharaterListCell
        cell.bindViewModel(present: presenter, index: indexPath.row)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2.5, height: 250)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if presenter.isLastPage {
            return CGSize.zero            
        } else {
            return CGSize(width: collectionView.frame.width, height: 40)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 5, right: 10)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let supplementaryView: UICollectionReusableView
        
        switch kind {
        case CSStickyHeaderParallaxHeader:
            supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CSStickyHeaderParallaxHeader, for: indexPath)
            if let view = supplementaryView as? MovieSortHeaderView {
                view.present = presenter
            }
        case UICollectionElementKindSectionFooter:
            supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: defaultSectionFooterIdentifier, for: indexPath)
            if let view = supplementaryView as? MTCollectionReusableView {
                if presenter.isLoading == true {
                    view.indicator.startAnimating()
                } else {
                    view.indicator.stopAnimating()
                }
            }

        default:
            supplementaryView = super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
        return supplementaryView
    }
}

// UICollectionViewDelegate
extension MCharaterListViewController {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.gotoDetailPage(index: indexPath.row)
    }
}
