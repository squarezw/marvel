//
//  MoviewListCell.swift
//  movie
//

import UIKit

class MCharaterListCell: MImageCoverCell {
    
    weak var present: MDiscoverListPresenter?
    
    var index: Int?
    
    lazy var favouriteButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ico_bglight_heart"), for: .normal)
        button.setImage(UIImage(named: "ico_heart"), for: .selected)
        button.addTarget(self, action: #selector(favourite), for: .touchUpInside)
        return button
    }()
    
    override func setup() {
        super.setup()
//        addSubview(favouriteButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
//        favouriteButton.snp.makeConstraints { (make) in
//            make.right.equalToSuperview()
//            make.width.height.equalTo(30)
//            make.top.equalTo(titleLabel)
//        }
    }

    func bindViewModel(present: MDiscoverListPresenter, index: Int) {
        self.present = present
        self.index = index
        let viewModel = present.items[index]
        
        titleLabel.text = viewModel.title
        imageView.sd_setImage(with: viewModel.posterUrl, placeholderImage: UIImage(named: "placeholder-movieimage"))
    }
    
    func favourite() {
        favouriteButton.isSelected = true
//        if let index = index {
//            present?.favourite(index: index)
//        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        index = nil
//        favouriteButton.isSelected = false
    }
}
