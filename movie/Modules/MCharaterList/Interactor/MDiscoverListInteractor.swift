//
//  MCharaterListInteractor.swift
//
//

import Foundation
import ObjectMapper
import RealmSwift

class MDiscoverListInteractor {
    weak var presenter: MDiscoverListPresenter?
    var provider: MTQuery = MTQuery.shared
    var isLoading: Bool = false
    var isLastPage: Bool = false
    
    public func orderBy(_ key: String) {
        self.fetchData(["sort_by": key])
    }
    
    public func searchBy(_ key: String) {
        self.fetchData(["nameStartsWith": key])
    }
    
    func fetchData(_ params: [String: String]? = nil) {
        if self.isLoading == true {
            return
        }
        
        self.isLoading = true
        // 3
        provider.getObject(APIResources.discover, params: params) { [weak self] (object, error) in
            if let strongSelf = self {
                
                if let datastring = object {
                    let results = Mapper<DataContainerTMDB<MovieBase>>().map(JSONString: datastring)?.results
                    
                    if let results = results {
                        strongSelf.presenter?.setData(items: results)
                    } else {
                        strongSelf.presenter?.loadDataError(error: MTError(internalError: .badParse))
                    }
                    
                } else {
                    strongSelf.presenter?.loadDataError(error: MTError(internalError: .badResponse))
                }

            }
            
            self?.isLoading = false
        }
    }
    
    // load next page
    public func loadMore(page: Int) {
        if self.isLoading == true {
            return
        }
        
        self.isLoading = true
        
        provider.getObject(APIResources.discover, params: ["page": String(page)]) { [weak self] (object, error) in
            if let strongSelf = self, let dataString = object {
                
                if let data = Mapper<DataContainerTMDB<MovieBase>>().map(JSONString: dataString) {
                    if let results = data.results, results.count > 0 {
                        strongSelf.presenter?.appendData(items: results)
                    } else {
                        strongSelf.isLastPage = true
                    }
                }
            }
            
            self?.isLoading = false
        }
        
    }
 
}
