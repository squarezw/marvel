//
//  MCharaterListInteractor.swift
//
//

import Foundation
import ObjectMapper
import RealmSwift

class MCharaterListInteractor {
    weak var presenter: MCharaterListPresenter?
    var provider: MTQuery = MTQuery.shared
    var isLoading: Bool = false
    var isLastPage: Bool = false
    
    public func orderBy(_ key: String) {
        self.fetchData(["orderBy": key])
    }
    
    public func searchBy(_ key: String) {
        self.fetchData(["nameStartsWith": key])
    }
    
    func fetchData(_ params: [String: String]? = nil) {
        if self.isLoading == true {
            return
        }
        
        self.isLoading = true
        // 3
        provider.getObject(APIResources.characters, params: params) { [weak self] (object, error) in
            if let strongSelf = self, let datastring = object {
                
                if let results = Mapper<DataContainer<Character>>().map(JSONString: datastring)?.results, results.count > 0 {
                    strongSelf.presenter?.setData(items: results)
                }
                
            }
            
            self?.isLoading = false
        }
    }
    
    // load next page
    public func loadMore(page: Int, limit: Int) {
        if self.isLoading == true {
            return
        }
        
        self.isLoading = true
        
        let offset = self.offset(page: page, limit: limit)
        provider.getObject(APIResources.characters, params: ["offset": String(offset)]) { [weak self] (object, error) in
            if let strongSelf = self, let dataString = object {
                
                if let data = Mapper<DataContainer<Character>>().map(JSONString: dataString) {
                    if data.offset + data.count >= data.total {
                        strongSelf.isLastPage = true
                    }
                    
                    if let results = data.results, results.count > 0 {
                        strongSelf.presenter?.appendData(items: results)
                    }
                }
            }
            
            self?.isLoading = false
        }
        
    }
    
    // TODO: Local data persist
    public func favourite(character: Character) {
        let realm = try! Realm()
        
        character.isFavourite = 1
        try! realm.write {
            realm.add(character)
        }
    }
    
    private func offset(page: Int, limit: Int) -> Int {
        if page > 1 {
            return (page - 1) * limit
        }
        return 0
    }
    
}
