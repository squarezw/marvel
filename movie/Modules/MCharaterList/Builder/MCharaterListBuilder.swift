//
//  MCharaterListBuilder.swift
//
//

import UIKit

struct MCharaterListBuilder {
    // VIPER
    static func build() -> UIViewController {
        // R
        let router = MTRoute.shared
        
        // I
        let interactor = MDiscoverListInteractor()

        // P
        let presenter = MDiscoverListPresenter(interactor: interactor)
        presenter.router = router
        
        // V
        let vc = MCharaterListViewController(presenter: presenter)
        presenter.view = vc
        
        interactor.presenter = presenter
        
        return vc
    }
}
