//
//  MCharaterDetailInteractor.swift
//
//

import Foundation
import ObjectMapper

class MCharaterDetailInteractor {
    weak var presenter: MCharaterDetailPresenter?
    var provider: MTQuery = MTQuery.shared
    var isLoading: Bool = false
    
    public func fetchData(_ type: MDetailResourceType, collectionURI uri: String, params: [String: String]? = nil) {
        
        self.isLoading = true

        provider.getObject(uri, params: params) { [weak self] (object, error) in
            if let strongSelf = self, let datastring = object {
                
                if let results = Mapper<DataContainer<CommonModel>>().map(JSONString: datastring)?.results, results.count > 0 {
                    strongSelf.presenter?.setItems(type, items: results)
                }
                
            }
            
            self?.isLoading = false
        }
    }
    
    // Local data persist
    public func favourite(character: Character) {
        character.isFavourite = 1
    }
}
