//
//  MCharaterDetailBuilder.swift
//
//

import UIKit

struct MCharaterDetailBuilder {
    // VIPER
    static func build(character: Character) -> UIViewController {
        // R
        let router = MTRoute.shared
        
        // I
        let interactor = MCharaterDetailInteractor()
        
        // P
        let presenter = MCharaterDetailPresenter(interactor: interactor, character: character)
        presenter.router = router
        
        // V
        let vc = MCharaterDetailViewController(presenter: presenter)
        presenter.view = vc
        
        interactor.presenter = presenter
        
        return vc
    }
}
