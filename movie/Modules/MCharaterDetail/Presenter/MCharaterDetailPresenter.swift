//
//  MCharaterDetailPresenter.swift
//
//

import Foundation

enum MDetailResourceType {
    case comics
    case series
    case stories
    case events
}

class MCharaterDetailPresenter {
    weak var view: ViewPrototol?
    weak var router: MTRoute?
    var interactor: MCharaterDetailInteractor
    var character: Character
    
    var headerModel: MImageCellModelProtocol?
    var comicsCollectionURI: String?
    var seriesCollectionURI: String?
    var storiesCollectionURI: String?
    var eventsCollectionURI: String?
    
    var comics: [MImageCellModel] = []
    var series: [MImageCellModel] = []
    var stories: [MImageCellModel] = []
    var events: [MImageCellModel] = []
    
    var items: [[MImageCellModel]] {
        return [[MImageCellModel(title: "Comics")], comics, [MImageCellModel(title: "Series")], series, [MImageCellModel(title: "Stories")], stories, [MImageCellModel(title: "Events")], events]
    }
    
    var isFavourited: Bool {
        get {
            return character.isFavourite == 1
        }
    }
    
    var isLoading: Bool {
        get {
            return self.interactor.isLoading
        }
    }

    // MARK: Initialize
    init(interactor: MCharaterDetailInteractor, character: Character) {
        self.interactor = interactor
        self.character = character
        setHeaderModel(item: character)
    }
    
    // MARK: public methods
    public func viewIsReady() {
        if let uri = comicsCollectionURI {
            self.interactor.fetchData(.comics, collectionURI: uri, params: ["limit":"3"])
            view?.isFetchingData()
        }
        if let uri = seriesCollectionURI {
            self.interactor.fetchData(.series, collectionURI: uri, params: ["limit":"3"])
            view?.isFetchingData()
        }
        if let uri = storiesCollectionURI {
            self.interactor.fetchData(.stories, collectionURI: uri, params: ["limit":"3"])
            view?.isFetchingData()
        }
        if let uri = eventsCollectionURI {
            self.interactor.fetchData(.events, collectionURI: uri, params: ["limit":"3"])
            view?.isFetchingData()
        }
    }
    
    public func setItems(_ type: MDetailResourceType, items: [CommonModel]) {
        switch type {
        case .comics:
            comics = convertModel(items: items)
        case .series:
            series = convertModel(items: items)
        case .stories:
            stories = convertModel(items: items)
        case .events:
            events = convertModel(items: items)
        }
        view?.completedFetch()
        self.view?.reloadData()
    }
    
    // favourite
    public func favourite() {
        interactor.favourite(character: character)
    }
    
    private func setHeaderModel(item: Character) {
        if let returned = item.comics?.returned, returned > 0 {
            comicsCollectionURI = item.comics?.collectionURI
        }
        if let returned = item.series?.returned, returned > 0 {
            seriesCollectionURI = item.series?.collectionURI
        }
        if let returned = item.stories?.returned, returned > 0 {
            storiesCollectionURI = item.stories?.collectionURI
        }
        if let returned = item.events?.returned, returned > 0 {
            eventsCollectionURI = item.events?.collectionURI
        }
        
        guard let urlString = item.thumbnail?.fullUrl(type: ImageVariants.portraitUncanny),
            let name = item.name else {
                return
        }
        self.headerModel = MImageCellModel(title: name, poster: urlString, popularityDesc: item.desc ?? "")
        self.view?.reloadData()
    }
    
    private func convertModel(items: [CommonModel]) -> [MImageCellModel] {
        var array: [MImageCellModel] = [MImageCellModel]()
        for item in items {
            guard let urlString = item.thumbnail?.fullUrl(type: ImageVariants.portraitUncanny),
                let name = item.title else {
                    continue
            }
            array.append(MImageCellModel(title: name, poster: urlString, popularityDesc: ""))
        }
        return array
    }
    
}
