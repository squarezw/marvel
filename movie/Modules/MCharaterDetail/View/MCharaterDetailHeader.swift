//
//  MCharaterDetailHeader.swift
//
//

import UIKit

class MCharaterDetailHeader: MImageCoverCell {
    
    weak var present: MCharaterDetailPresenter?
    
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    lazy var favouriteButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ico_bglight_heart"), for: .normal)
        button.setImage(UIImage(named: "ico_heart"), for: .selected)
        button.addTarget(self, action: #selector(favourite), for: .touchUpInside)
        return button
    }()
    
    var viewModel: MImageCellModelProtocol? {
        didSet {
            bindViewModel()
        }
    }
    
    func bindViewModel() {
        titleLabel.text = viewModel?.title
        descLabel.text = viewModel?.popularityDesc
        imageView.sd_setImage(with: viewModel?.posterUrl, placeholderImage: UIImage(named: "placeholder-movieimage"))
        favouriteButton.isSelected = present?.isFavourited ?? false
    }

    override func setup() {
        addSubview(descLabel)
        addSubview(favouriteButton)
        super.setup()
        self.clipsToBounds = true
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        var height: CGFloat = 0.0
        if let desc = descLabel.text, !desc.isEmpty {
            height = 30 + desc.height(descLabel.font, constrainedWidth: imageView.frame.width)
        }
        
        imageView.snp.updateConstraints { (make) in
            make.height.equalToSuperview().offset(-30.0 - height)
        }
        
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.right.equalTo(imageView)
            make.height.equalTo(height)
        }
        
        favouriteButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.width.height.equalTo(30)
            make.top.equalTo(titleLabel)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        imageView.image = nil
        descLabel.text = nil
        favouriteButton.isSelected = false
    }
    
    func favourite() {
        favouriteButton.isSelected = !favouriteButton.isSelected
    }
}
