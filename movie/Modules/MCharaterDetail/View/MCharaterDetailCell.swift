//
//  MCharaterDetailCell.swift
//
//

import UIKit

class MCharaterDetailCell: MImageCoverCell {
    
    var viewModel: MImageCellModel? {
        didSet {
            bindViewModel()
        }
    }
    
    func bindViewModel() {
        titleLabel.text = viewModel?.title
        imageView.sd_setImage(with: viewModel?.posterUrl, placeholderImage: UIImage(named: "placeholder-movieimage"))
    }
    
    override func setup() {
        super.setup()
        titleLabel.textColor = UIColor.white
        titleLabel.layer.shadowOffset = CGSize(width: 0, height: 2)
        titleLabel.layer.shadowOpacity = 0.5
        titleLabel.layer.shouldRasterize = true;
        titleLabel.layer.rasterizationScale = UIScreen.main.scale;
        titleLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    override func setupConstraints() {        
        imageView.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
            make.height.equalTo(25)
        }
    }
    
}
