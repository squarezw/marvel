//
//  MCharaterDetailPresenter.swift
//
//

import Foundation

class MMovieDetailPresenter {
    weak var view: ViewPrototol?
    weak var router: MTRoute?
    var interactor: MMovieDetailInteractor
    var movie: MovieBase
    var headerModel: MImageCellModel?
    var bookingLink: String?
    
    var synopsis: [MImageCellModel] = []
    var genres: [MImageCellModel] = []
    var language: [MImageCellModel] = []
    var duration: [MImageCellModel] = []
    
    var items: [[MImageCellModel]] {
        return [[MImageCellModel(title: "Synopsis")], synopsis,
                [MImageCellModel(title: "Genres")], genres,
                [MImageCellModel(title: "Language")], language,
                [MImageCellModel(title: "Duration")], duration]
    }
    
    var isLoading: Bool {
        get {
            return self.interactor.isLoading
        }
    }

    // MARK: Initialize
    init(interactor: MMovieDetailInteractor, movie: MovieBase) {
        self.interactor = interactor
        self.movie = movie
    }
    
    // MARK: public methods
    public func viewIsReady() {
        self.interactor.fetchData(movie: movie)
        view?.isFetchingData()
    }
    
    public func setMovie(_ movie: Movie) {
        if let overview = movie.overview {
            synopsis = [MImageCellModel(title: overview)]
        }
        if let genresDesc = movie.genresDesc {
            genres = [MImageCellModel(title: genresDesc)]
        }
        if let lang = movie.language {
            language = [MImageCellModel(title: lang)]
        }
        if let dura = movie.duration {
            duration = [MImageCellModel(title: dura)]
        }
        if let title = movie.title, let poster = movie.posterUrlString {
            headerModel = MImageCellModel(title: title, poster: poster, popularityDesc: "")
        }
        bookingLink = movie.homepage
        view?.completedFetch()
        self.view?.reloadData()
    }
    
    public func gotoBooking() {
        guard let link = bookingLink else {
            return
        }
        self.router?.gotoBookingPage(link: link)
    }
    
}
