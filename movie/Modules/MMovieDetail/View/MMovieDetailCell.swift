//
//  MCharaterDetailCell.swift
//
//

import UIKit

class MMovieDetailCell: MImageCoverCell {
    
    var viewModel: MImageCellModel? {
        didSet {
            bindViewModel()
        }
    }
    
    func bindViewModel() {
        titleLabel.text = viewModel?.title
    }
    
    override func setup() {
        super.setup()
        titleLabel.textColor = UIColor.white
        titleLabel.layer.shadowOffset = CGSize(width: 0, height: 2)
        titleLabel.layer.shadowOpacity = 0.5
        titleLabel.layer.shouldRasterize = true
        titleLabel.layer.rasterizationScale = UIScreen.main.scale
        titleLabel.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.clipsToBounds = true
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().offset(5)
        }
    }
    
}
