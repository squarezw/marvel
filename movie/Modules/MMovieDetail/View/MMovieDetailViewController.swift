//
//  MCharaterDetailViewController.swift
//
//

import UIKit
import CSStickyHeaderFlowLayout

class MMovieDetailViewController: MTCollectionViewController {
    
    var presenter: MMovieDetailPresenter
    
    let spaceCell = "UICollectionViewSpaceCell"
    
    init(presenter: MMovieDetailPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        self.allowPullRefresh = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configCollectionView() {
        super.configCollectionView()
        self.layout.parallaxHeaderReferenceSize = CGSize(width: self.view.frame.width, height: 240)
        self.layout.parallaxHeaderMinimumReferenceSize = CGSize(width: self.view.frame.width, height: 40)
        collectionView.register(MMovieDetailCell.self, forCellWithReuseIdentifier: defaultUICollectionViewCell)
        collectionView.register(MTCollectionSectionTitleView.self, forCellWithReuseIdentifier: spaceCell)
        collectionView.register(MMovieDetailHeader.self, forSupplementaryViewOfKind: CSStickyHeaderParallaxHeader, withReuseIdentifier: CSStickyHeaderParallaxHeader)
        collectionView.register(MTCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: defaultSectionFooterIdentifier)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detail"

        // 1
        presenter.viewIsReady()

        view.addSubview(backButton)
        
        activityIndicator.frame = CGRect(x: view.frame.width - 30, y: 10, width: 30, height: 30)
        view.addSubview(activityIndicator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension MMovieDetailViewController {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.items[section].count 
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section % 2 == 0 {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: spaceCell, for: indexPath) as? MTCollectionSectionTitleView {
                cell.titleLabel.text = presenter.items[indexPath.section][indexPath.row].title
                return cell
            }
        } else {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: defaultUICollectionViewCell, for: indexPath) as? MMovieDetailCell {
                cell.viewModel = presenter.items[indexPath.section][indexPath.row]
                return cell
            }
        }
        return collectionView.dequeueReusableCell(withReuseIdentifier: defaultUICollectionViewCell, for: indexPath)
    }
    
    // Cell Size
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: indexPath.section % 2 == 0 ? 50 : 120)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let supplementaryView: UICollectionReusableView
        
        switch kind {
        case CSStickyHeaderParallaxHeader:
            supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CSStickyHeaderParallaxHeader, for: indexPath)
            if let view = supplementaryView as? MMovieDetailHeader {
                view.viewModel = presenter.headerModel
                view.present = presenter
            }
            return supplementaryView
        default:
            break
        }
        return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
    }
}
