//
//  MCharaterDetailHeader.swift
//
//

import UIKit

class MMovieDetailHeader: MImageCoverCell {
    
    weak var present: MMovieDetailPresenter?
    
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    lazy var bookButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Book", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(book), for: .touchUpInside)
        button.layer.borderColor = UIColor.blue.cgColor
        button.layer.borderWidth = 2.0
        button.layer.backgroundColor = UIColor.gray.cgColor
        return button
    }()
    
    var viewModel: MImageCellModelProtocol? {
        didSet {
            bindViewModel()
        }
    }
    
    func book() {
        present?.gotoBooking()
    }
    
    func bindViewModel() {
        titleLabel.text = viewModel?.title
        descLabel.text = viewModel?.popularityDesc
        imageView.sd_setImage(with: viewModel?.posterUrl, placeholderImage: UIImage(named: "placeholder-movieimage"))
    }

    override func setup() {
        super.setup()
        addSubview(descLabel)
        addSubview(bookButton)
        self.clipsToBounds = true
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        var height: CGFloat = 0.0
        if let desc = descLabel.text, !desc.isEmpty {
            height = 30 + desc.height(descLabel.font, constrainedWidth: imageView.frame.width)
        }
        
        imageView.snp.updateConstraints { (make) in
            make.height.equalToSuperview().offset(-30.0 - height)
        }
        
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom)
            make.left.right.equalTo(imageView)
            make.height.equalTo(height)
        }
        
        bookButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.height.equalTo(35)
            make.right.equalToSuperview()
            make.width.equalTo(50)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        imageView.image = nil
        descLabel.text = nil
    }
}
