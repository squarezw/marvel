//
//  MCharaterDetailInteractor.swift
//
//

import Foundation
import ObjectMapper

class MMovieDetailInteractor {
    weak var presenter: MMovieDetailPresenter?
    var provider: MTQuery = MTQuery.shared
    var isLoading: Bool = false
    
    public func fetchData(movie: MovieBase, params: [String: String]? = nil) {
        
        self.isLoading = true
        
        guard let id = movie.id else {
            self.isLoading = false
            return
        }

        provider.getObject(APIResources.movie.uri(withId: String(id)), params: params) { [weak self] (object, error) in
            if let strongSelf = self, let datastring = object {
                
                if let movie = Mapper<Movie>().map(JSONString: datastring) {
                    strongSelf.presenter?.setMovie(movie)
                }
                
            }
            
            self?.isLoading = false
        }
    }
}
