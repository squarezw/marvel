//
//  MCharaterDetailBuilder.swift
//
//

import UIKit

struct MMovieDetailBuilder {
    // VIPER
    static func build(movie: MovieBase) -> UIViewController {
        // R
        let router = MTRoute.shared
        
        // I
        let interactor = MMovieDetailInteractor()
        
        // P
        let presenter = MMovieDetailPresenter(interactor: interactor, movie: movie)
        presenter.router = router
        
        // V
        let vc = MMovieDetailViewController(presenter: presenter)
        presenter.view = vc
        
        interactor.presenter = presenter
        
        return vc
    }
}
