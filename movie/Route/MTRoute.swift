//
//  MTRoute.swift
//
//

import UIKit

class MTRoute {
    
    static let shared = MTRoute()
    
    lazy var rootVC: UINavigationController = {
        let navc = UINavigationController(rootViewController: MCharaterListBuilder.build())
        navc.delegate = MTAnimatorEngine.shared
        return navc
    }()
    
    func gotoDetailPage(character: Character) {
        self.rootVC.pushViewController(MCharaterDetailBuilder.build(character: character), animated: true)
    }
    
    func gotoMovieDetailPage(movie: MovieBase) {
        self.rootVC.pushViewController(MMovieDetailBuilder.build(movie: movie), animated: true)
    }
    
    func gotoBookingPage(link: String) {
        let webController = MTWebViewController(url: link)
        self.rootVC.pushViewController(webController, animated: true)
    }
    
}
