//
//  ImageModel.swift
//
//

import Foundation
import ObjectMapper

enum ImageVariants: String {
    // Portrait aspect ratio
    case portraitSmall          = "portrait_small" //50x75px
    case portraitMedium         = "portrait_medium" //100x150px
    case portraitXlarge         = "portrait_xlarge" //150x225px
    case portraitFantastic      = "portrait_fantastic" //168x252px
    case portraitIncredible     = "portrait_incredible" //216x324px
    case portraitUncanny        = "portrait_uncanny" //300x450px
    // Standard (square) aspect ratio
    case standardSmall         = "standard_small" //65x45px
    case standardMedium        = "standard_medium" //100x100px
    case standardLarge         = "standard_large" //140x140px
    case standardXlarge        = "standard_xlarge" //200x200px
    case standardFantastic     = "standard_fantastic" //250x250px
    case standardAmazing       = "standard_amazing" //180x180px
    // Landscape aspect ratio
    case landscapeSmall        = "landscape_small" //120x90px
    case landscapeMedium       = "landscape_medium" //175x130px
    case landscapeLarge        = "landscape_large" //190x140px
    case landscapeXlarge       = "landscape_xlarge" //270x200px
    case landscapeAmazing      = "landscape_amazing" //250x156px
    case landscapeIncredible   = "landscape_incredible" //464x261px
}


class ImageModel: Mappable {
    var path: String?
    var ext: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        path <- map["path"]
        ext <- map["extension"]
    }
    
    func fullUrl(type: ImageVariants) -> String? {
        guard let path = path, let ext = ext  else {
            return nil
        }
        return "\(path)/\(type.rawValue).\(ext)"
    }
    
}
