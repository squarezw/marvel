//
//  CommonModel.swift
//
//

import Foundation
import ObjectMapper

class CommonModel: Mappable {
    var id: Int?
    var title: String?
    var thumbnail: ImageModel?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        thumbnail <- map["thumbnail"]
    }
}
