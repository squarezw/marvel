//
//  Character.swift
//
//

import Foundation
import ObjectMapper
import ObjectMapper_Realm
import RealmSwift

class Character: Object, Mappable {
    dynamic var isFavourite = 0
    
    var id: Int?
    var name: String?
    var desc: String?
    var thumbnail: ImageModel?
    var comics: DataCollection?
    var series: DataCollection?
    var stories: DataCollection?
    var events: DataCollection?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        desc <- map["description"]
        thumbnail <- map["thumbnail"]
        comics <- map["comics"]
        series <- map["series"]
        stories <- map["stories"]
        events <- map["events"]
    }
    
}
