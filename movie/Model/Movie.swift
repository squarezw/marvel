//
//  Movie.swift
//
//

import Foundation
import ObjectMapper

class MovieBase: Mappable {
    var id: Int?
    var title: String?
    var posterPath: String?
    var adult: Bool?
    var overview: String?
    var releaseDate: String?
    var runtime: Int?
    var genreIds: [Int]?
    var originalTitle: String?
    var originalLanguage: String?
    var backdropPath: String?
    var popularity: Float?
    var voteCount: Int?
    var video: Bool?
    var voteAverage: Float?
    
    required init?(map: Map) {
        if map.JSON["id"] == nil {
            return nil
        }
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        title               <- map["title"]
        posterPath          <- map["poster_path"]
        adult               <- map["adult"]
        overview            <- map["overview"]
        releaseDate         <- map["releaseDate"]
        runtime             <- map["runtime"]
        genreIds            <- map["genre_ids"]
        originalTitle       <- map["original_title"]
        originalLanguage    <- map["original_language"]
        backdropPath        <- map["backdrop_path"]
        popularity          <- map["popularity"]
        voteCount           <- map["voteCount"]
        video               <- map["video"]
        voteAverage         <- map["vote_average"]
    }
    
    var posterUrlString: String? {
        if let poster = posterPath {
            return APIConfig.fullImageUrlString(imagePath: poster)
        }
        return nil
    }

    var duration: String? {
        if let runtime = runtime {
            return "\(runtime / 60)h \(runtime % 60)m"
        }
        return nil
    }
}

class Movie: MovieBase {
    var genres: [MTDictionary]?
    var language: String?
    var homepage: String?
    
    var genresDesc: String? {
        if let genres = genres {
            return genres.flatMap({ (dict) -> String? in
                if let value = dict["name"] as? String {
                    return value
                }
                return nil
            }).joined(separator: ",")
        }
        return nil
    }
    
    var popularityDesc: String? {
        return String(format: "🎥 %.1f", popularity ?? "")
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        genres      <- map["genres"]
        language    <- map["spoken_languages.0.name"]
        homepage    <- map["homepage"]
    }
}
