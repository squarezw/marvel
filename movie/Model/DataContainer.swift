//
//  Generic.swift
//
//

import Foundation
import ObjectMapper

class DataWrapper: Mappable {
    var code: Int?
    var status: String?
    var copyright: String?
    var etag: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        status <- map["status"]
        copyright <- map["copyright"]
        etag <- map["etag"]
    }
    
}

class DataContainerTMDB<T: Mappable>: DataWrapper {
    var page: Int?
    var results: [T]?
    var total_results: Int?
    var total_pages: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        page <- map["page"]
        total_results <- map["total_results"]
        total_pages <- map["total_pages"]
        results <- map["results"]
    }
}

class DataContainer<T: Mappable>: DataWrapper {
    var offset: Int = 0
    var limit: Int = 20
    var total: Int = 0
    var count: Int = 0
    var results: [T]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        offset <- map["data.offset"]
        limit <- map["data.limit"]
        total <- map["data.total"]
        count <- map["data.count"]
        total <- map["data.total"]
        results <- map["data.results"]
    }
}

class DataCollection: Mappable {
    var available: Int?
    var collectionURI: String?
    var items: [Any]?
    var returned: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        available <- map["available"]
        collectionURI <- map["collectionURI"]
        items <- map["items"]
        returned <- map["returned"]
    }
    
}
